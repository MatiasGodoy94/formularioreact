import React, { Fragment, useState } from 'react'
import '../App.css';
import perri from "../img/perri.png";

function Formulario() {

    const [datos, setDatos] = useState({
        nombre: '',
        password: '',
        mascotas: ''
    })


    const handleInputChange = (event) => {
        console.log(event.target.value)
        if (event.target.type === 'checkbox' && !event.target.checked) {
            setDatos({
                ...datos, [event.target.name] : ''
            });
        } else {
            setDatos({
                ...datos,
                [event.target.name]: event.target.value
            });
        }
    }

    const { nombre, password,mascotas } = datos;

    const tieneMascotas = () => mascotas === 'on' ? 'tiene mascotas' : 'no tiene mascotas'

    const enviarDatos = (event) => {
        event.preventDefault();
         return   alert(`${datos.nombre} ${tieneMascotas()}`)
            
    }



    const habilitarBoton = () => {
        
        if (nombre === '' || password === '') {
            return true
        } else {
            return false
        }
    }

    const claseDeBoton = () => {
        
        if (nombre === '' || password === '') {
            return "boton-deshabilitado"
        }else{
            return "boton-habilitado"
        }
    }

    const [boton,setBoton] = useState(false)

    function negarBoton(){
        setBoton(!boton)
    }

    function cambiarTipo(){
        if(boton === false){
            return "text"
        }else{
            return "password"
        }
    }


    return (
        <Fragment>
            <img className="perri" src={perri}></img>
            <img className="perri2" src={perri}></img>
            <div className='container-form'>
                <h1>Formulario de mascotas</h1>
                <div className="form">
                    <form className="formulario" onSubmit={enviarDatos}>
                        <div className="datos">
                            <label><h3>Nombre Completo:</h3></label>
                            <input type="text"
                                placeholder="Ingrese su nombre"
                                name="nombre"
                                onChange={handleInputChange}>
                            </input>
                            <label><h3>Contraseña:</h3></label>
                            <input 
                                 type={cambiarTipo()}
                                placeholder="Ingrese su contraseña"
                                name="password"
                                onChange={handleInputChange}>
                            </input><br></br>
                            <button className="boton-habilitado" type="button" onClick={negarBoton}>Show/Hide</button>
                            

                            <label><h3>¿Posee Mascotas?</h3></label>
                            <input type="checkbox"
                                name="mascotas"
                                onChange={handleInputChange}>
                            </input><br></br>
                            
                            <button className={claseDeBoton()}  disabled={habilitarBoton()}>Enviar Datos</button>
                        </div>
                    </form>
                </div>
            </div>

        </Fragment>
    )
}

export default Formulario;